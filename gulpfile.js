var gulp = require('gulp'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    jshint_stylish = require('jshint-stylish'),
    uglify = require('gulp-uglify'),
    minifyCSS = require('gulp-minify-css'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename');

var version = 'v1';

gulp.task('js', function() {

    return gulp.src(['js/**/*.js', '!js/scripts.min*.js'])
                .pipe(jshint({
                    browser: true,
                    globals: {
                        'angular': true
                    }
                }))
                .pipe(jshint.reporter(jshint_stylish))
                .pipe(concat('scripts.min.' + version + '.js'))
                .pipe(uglify())
                .pipe(gulp.dest(__dirname + '/js/'));

});

gulp.task('css', function() {

    return gulp.src('css/styles.scss')
                .pipe(sass())
                .pipe(rename('styles.min.' + version + '.css'))
                .pipe(minifyCSS())
                .pipe(gulp.dest(__dirname + '/css/'));

});

gulp.task('default', ['js', 'css'], function() {

});