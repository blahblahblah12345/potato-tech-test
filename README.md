# Potato Tech Test

I completed the basic spec using SCSS and AngularJS. The only other library included is moment.js for parsing dates.

In addition to the basics, I included a build set-up (using gulp); a search box to filter photos based on tags; a loading animation; and the ability to click tags within the photo-detail view in order to see different photo feeds.