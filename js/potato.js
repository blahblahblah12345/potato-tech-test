
angular.module('potato', [ ])

    .controller('MainCtrl', ['getFeedItems', function(getFeedItems) {

        var main = this;

        main.tagFilter = '';
        main.results = null;
        main.selected = null;
        main.tag = 'potato';

        main.setTag = function(tag) {

            main.results = null;
            main.selected = null;
            main.tag = tag;

            refreshFeed();

        };

        function refreshFeed() {

            getFeedItems(main.tag, function(err, items) {

                if (err) {
                    // todo: handle err
                    return;
                }

                main.results = items;

            });

        }

        refreshFeed();

    }]);