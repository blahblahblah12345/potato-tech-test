
angular.module('potato')

    // filter for changing a flicker photo url 
    .filter('changeImgFormat', [function(){

        return function(text) {
            return text.replace(/_m.jpg/, '_q.jpg');
        };

    }])

    // convert dates into a human readable form with ordinal
    .filter('ordinalDate', [function() {

        return function(date) {
            return moment(date).format('Do MMM YYYY [at] hh:mm');
        };

    }])

    // filter to extract author's name from Flickr feed
    .filter('getAuthor', [function() {
        
        return function(author) {
            author = author || '';
            return author.replace(/^[^\(]*\(/,'').replace(/\)[^\)]*$/, '');
        };

    }]);