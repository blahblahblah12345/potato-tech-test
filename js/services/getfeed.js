
angular.module('potato')

    // service for getting flickr feed
    .factory('getFeedItems', ['$http', function($http) {

        return function(tag, callback) {

            var url = 'https://api.flickr.com/services/feeds/photos_public.gne?tags=' + 
                            encodeURIComponent(tag) + 
                            '&tagmode=all&format=json&jsoncallback=JSON_CALLBACK';

            $http.jsonp(url).success(function(data) {

                callback(null, data.items);

            }).error(function() {

                //todo: handle error

            });

        };

    }]);